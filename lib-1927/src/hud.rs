//! the head up display
//!
use gdnative::api::*;
use gdnative::prelude::*;
use strum::IntoEnumIterator; // 0.17.1
use strum_macros::Display;
use strum_macros::EnumIter;

/// this is the module
#[derive(NativeClass)]
#[inherit(Node2D)]
pub struct Hud {
    top_margin: f32,
    left_spacing: f32,
    left_height_spacing: f32,
    right_spacing: f32,
    current_level: i64,
}

#[derive(Debug, EnumIter, Display)]
enum LeftBoxes {
    Score,
    Combo,
    LevelName,
    LevelTimer,
    PlayerHealth,
}

#[derive(Debug, EnumIter, Display)]
enum RightBoxes {
    Level1 = 0,
    Level2,
    Level3,
    Level4,
    Level5,
    Level6,
}

/// Implementation of Hud
#[methods]
impl Hud {
    fn new(_owner: &Node2D) -> Self {
        Hud {
            top_margin: 20.0,
            left_spacing: 20.0,
            left_height_spacing: 20.0,
            right_spacing: 130.0,
            current_level: 0,
        }
    }

    #[method]
    fn _ready(&mut self, #[base] owner: TRef<Node2D>) {
        let win_size = owner.get_viewport_rect();
        godot_dbg!(win_size);

        let mut position_y = self.top_margin;
        for box_name in LeftBoxes::iter() {
            let name: String = box_name.to_string();
            let label = Label::new();
            label.set_name(&name);
            label.set_text(&name);
            label.add_color_override("font_color", Color::from_rgba(0.0, 0.0, 0.0, 1.0));
            label.set_position(
                Vector2 {
                    x: self.left_spacing,
                    y: position_y,
                },
                false,
            );
            owner.add_child(label, false);
            position_y += self.left_height_spacing;
        }

        position_y = self.top_margin;
        for box_name in RightBoxes::iter() {
            let name: String = box_name.to_string();
            let label = Label::new();
            label.set_name(&name);
            label.set_text(format!("{} : ", &name));
            label.add_color_override("font_color", Color::from_rgba(0.0, 0.0, 0.0, 1.0));
            label.set_position(
                Vector2 {
                    x: win_size.size.x - self.right_spacing,
                    y: position_y,
                },
                false,
            );
            owner.add_child(label, false);
            position_y += self.left_height_spacing;
        }

        let score_emitter = &mut owner.get_node("/root/World/Score").unwrap();
        let score_emitter = unsafe { score_emitter.assume_safe() };

        let main_emitter = &mut owner.get_node("/root/World").unwrap();
        let main_emitter = unsafe { main_emitter.assume_safe() };

        let player_emitter = &mut owner.get_node("/root/World/Player").unwrap();
        let player_emitter = unsafe { player_emitter.assume_safe() };

        godot_dbg!("Hud added");
        score_emitter
            .connect(
                "score_player_total",
                owner,
                "update_player_score",
                VariantArray::new_shared(),
                0,
            )
            .unwrap();
        score_emitter
            .connect(
                "score_player_combo",
                owner,
                "update_player_combo",
                VariantArray::new_shared(),
                0,
            )
            .unwrap();
        main_emitter
            .connect(
                "main_level_name",
                owner,
                "update_level",
                VariantArray::new_shared(),
                0,
            )
            .unwrap();
        main_emitter
            .connect(
                "main_level_timer",
                owner,
                "update_level_timer",
                VariantArray::new_shared(),
                0,
            )
            .unwrap();
        player_emitter
            .connect(
                "player_health",
                owner,
                "update_player_health",
                VariantArray::new_shared(),
                0,
            )
            .unwrap();
    }
    #[method]
    fn update_player_score(&mut self, #[base] owner: &Node2D, score: Variant) {
        let score = score.try_to::<i32>().unwrap();
        match unsafe { owner.get_node_as::<Label>("Score") } {
            Some(block) => block.set_text(format!("Score: {score}")),
            None => godot_warn!("block not found"),
        }

        if self.current_level >= 0 {
            let mut box_right_iter = RightBoxes::iter();
            //let mut test = box_right_iter.next().unwrap().to_string();
            let level = box_right_iter
                .nth(self.current_level as usize)
                .unwrap()
                .to_string();

            // check the correct label via current level
            match unsafe { owner.get_node_as::<Label>(&level) } {
                Some(label) => label.set_text(format!("{level}: {score}")),
                None => godot_warn!("label not found"),
            }
        } else {
            for box_name in RightBoxes::iter() {
                let level: String = box_name.to_string();
                match unsafe { owner.get_node_as::<Label>(&level) } {
                    Some(label) => label.set_text(format!("{level}: 0")),
                    None => godot_warn!("label not found"),
                }
            }
        }
    }
    #[method]
    fn update_player_combo(&mut self, #[base] owner: &Node2D, combo: Variant) {
        let combo = combo.try_to::<i32>().unwrap();
        match unsafe { owner.get_node_as::<Label>("Combo") } {
            Some(block) => block.set_text(format!("Combo: {combo}")),
            None => println!("block not found"),
        }
    }
    #[method]
    fn update_level(&mut self, #[base] owner: &Node2D, level_name: Variant) {
        let level_name = level_name.try_to::<String>().unwrap();
        match unsafe { owner.get_node_as::<Label>("LevelName") } {
            Some(block) => block.set_text(format!("LevelName: {level_name}")),
            None => println!("block not found"),
        }
    }
    #[method]
    fn update_level_timer(&mut self, #[base] owner: &Node2D, level_timer: Variant) {
        let level_timer = level_timer.try_to::<f64>().unwrap() as i64;
        match unsafe { owner.get_node_as::<Label>("LevelTimer") } {
            Some(block) => block.set_text(format!("LevelTimer: {level_timer}")),
            None => println!("block not found"),
        }
    }
    #[method]
    fn update_player_health(&mut self, #[base] owner: &Node2D, player_health: Variant) {
        let player_health = player_health.try_to::<i64>().unwrap();
        match unsafe { owner.get_node_as::<Label>("PlayerHealth") } {
            Some(block) => block.set_text(format!("PlayerHealth: {player_health}")),
            None => println!("player_health not found"),
        }
    }
    #[method]
    pub fn start(&mut self, #[base] owner: &Node2D) {
        self.current_level = -1;
        self.update_player_score(owner, 0.to_variant());
        self.update_player_combo(owner, 1.to_variant());
    }
    #[method]
    pub fn set_level(&mut self, #[base] _owner: &Node2D, level: i64) {
        self.current_level = level;
    }
}
