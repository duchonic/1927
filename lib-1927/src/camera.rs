use std::f32::consts::PI;

use gdnative::prelude::*;

/// The Camera
///
///
#[derive(NativeClass)]
#[inherit(Spatial)]
pub struct Camera {}

#[methods]
impl Camera {
    fn new(_owner: &Spatial) -> Self {
        Camera {}
    }
    #[method]
    fn _ready(&mut self, #[base] owner: TRef<Spatial>) {
        godot_dbg!("Camera added");

        owner.rotate_x((-PI * 1.0 / 8.0) as f64);
        owner.rotate_x((-PI * 1.0 / 8.0) as f64);

        let emitter = &mut owner.get_node("/root/World/Player").unwrap();
        let emitter = unsafe { emitter.assume_safe() };

        emitter
            .connect(
                "player_position",
                owner,
                "camera_position",
                VariantArray::new_shared(),
                0,
            )
            .unwrap();
    }

    #[method]
    fn _process(&mut self, #[base] owner: &Spatial, _delta: f32) {
        let input = Input::godot_singleton();

        if Input::is_action_just_pressed(input, "view_a", false) {
            owner.rotate_x((-PI * 1.0 / 8.0) as f64);
        }
        if Input::is_action_just_pressed(input, "view_b", false) {
            owner.rotate_x((PI * 1.0 / 8.0) as f64);
        }
        if Input::is_action_just_pressed(input, "view_rear", false) {
            owner.rotate_y(PI as f64);
        }
    }

    #[method]
    fn camera_position(&mut self, #[base] owner: &Spatial, data: Variant) {
        let position = data.try_to::<Vector3>().unwrap();
        let mut camera_position = owner.translation();
        camera_position.z = position.z;
        owner.set_translation(camera_position);
    }
}
