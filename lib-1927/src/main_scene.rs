use crate::enemy;
use crate::ground;
use crate::hud;
use crate::player;
use crate::score;

use gdnative::prelude::*;
use serde_json::*;

#[derive(Debug, PartialEq)]
enum GameState {
    TitleScreen,
    Start,
    Running,
    LevelDone,
    Score,
    GameDone,
    Setting,
}

#[derive(Debug, FromVariant)]
pub struct Colors {
    side_color: Color,
    ground_color: Color,
    sky_color: Color,
}

#[derive(Debug, FromVariant)]
pub struct LevelData {
    location: String,
    enemies: i64,
    player_speed: i64,
    color: Colors,
    boss_strength: i64,
    blocks: i32,
}

#[derive(Debug)]
pub struct LevelInfo {
    actual: i64,
    last: i64,
}

#[derive(NativeClass)]
#[inherit(Spatial)]
#[derive(Debug)]
#[register_with(Self::register_signals)]
pub struct Main {
    score: i64,
    state: GameState,
    level_timer: f64,
    level_data: Vec<LevelData>,
    level_info: LevelInfo,
    filepath: String,
}

#[methods]
impl Main {
    fn register_signals(signal_builder: &ClassBuilder<Self>) {
        signal_builder.signal("main_level_name").done();
        signal_builder.signal("main_level_timer").done();
    }

    fn new(_owner: &Spatial) -> Self {
        Main {
            score: 0,
            state: GameState::TitleScreen,
            level_timer: 0.0,
            level_data: Vec::new(),
            level_info: LevelInfo { actual: 0, last: 0 },
            filepath: "res://data/level.json".to_string(),
        }
    }

    #[method]
    fn _ready(&mut self, #[base] _owner: &Spatial) {
        let level =
            { serde_json::from_str::<Value>(&self.load_data_as_string(&self.filepath)).unwrap() };

        let number_levels = level["level"].as_array().unwrap().len();

        for index in 0..number_levels {
            self.level_info.last += 1;
            let location = level["level"][index]["location"]
                .as_str()
                .unwrap()
                .to_string();
            let enemies = match level["level"][index]["enemies"].as_i64() {
                Some(enemies) => enemies,
                None => {
                    godot_warn!("enemies not found");
                    10
                }
            };
            let boss_strength = level["level"][index]["boss_strength"].as_i64().unwrap();
            let mut ground_color = Color::from_rgba(0.0, 1.0, 0.5, 1.0);
            ground_color.r = match level["level"][index]["ground_color"]["red"].as_f64() {
                Some(color) => color as f32,
                None => 0.0,
            };
            ground_color.g = match level["level"][index]["ground_color"]["green"].as_f64() {
                Some(color) => color as f32,
                None => 0.0,
            };
            ground_color.b = match level["level"][index]["ground_color"]["blue"].as_f64() {
                Some(color) => color as f32,
                None => 1.0,
            };
            let mut side_color = Color::from_rgba(1.0, 1.0, 0.5, 1.0);
            side_color.r = match level["level"][index]["side_color"]["red"].as_f64() {
                Some(color) => color as f32,
                None => 0.0,
            };
            side_color.g = match level["level"][index]["side_color"]["green"].as_f64() {
                Some(color) => color as f32,
                None => 1.0,
            };
            side_color.b = match level["level"][index]["side_color"]["blue"].as_f64() {
                Some(color) => color as f32,
                None => 0.0,
            };
            let mut sky_color = Color::from_rgba(0.0, 1.0, 0.5, 1.0);
            sky_color.r = match level["level"][index]["sky_color"]["red"].as_f64() {
                Some(color) => color as f32,
                None => 0.0,
            };
            sky_color.g = match level["level"][index]["sky_color"]["green"].as_f64() {
                Some(color) => color as f32,
                None => 0.0,
            };
            sky_color.b = match level["level"][index]["sky_color"]["blue"].as_f64() {
                Some(color) => color as f32,
                None => 0.0,
            };

            let color: Colors = Colors {
                ground_color,
                side_color,
                sky_color,
            };

            let blocks = match level["level"][index]["blocks"].as_i64() {
                Some(color) => color as i32,
                None => 0,
            };

            let level_data = LevelData {
                location,
                enemies,
                player_speed: level["level"][index]["player_speed"].as_i64().unwrap(), //player_speed,
                color,
                boss_strength,
                blocks,
            };
            godot_dbg!(&level_data);
            self.level_data.push(level_data);
        }
    }

    #[method]
    fn _process(&mut self, #[base] owner: &Spatial, delta: f64) {
        let input = Input::godot_singleton();
        self.state = match self.state {
            GameState::TitleScreen => self.titlescreen_state(owner, input),
            GameState::Start => self.start_state(owner),
            GameState::Running => self.running_state(owner, input, delta),
            GameState::LevelDone => self.leveldone_state(owner),
            GameState::Score => self.score_state(owner, input),
            GameState::GameDone => self.game_done_state(owner, input),
            GameState::Setting => GameState::TitleScreen,
        };
    }

    #[method]
    fn game_over(&self, #[base] _owner: &Spatial) {
        godot_print!("score {}", self.score);
    }

    #[method]
    fn new_game(&mut self, #[base] _owner: &Spatial) {
        self.score = 0;
    }

    fn titlescreen_state(&mut self, _owner: &Spatial, input: &Input) -> GameState {
        if Input::is_action_just_pressed(input, "ui_accept", false)
            || Input::is_action_just_pressed(input, "ui_shot", false)
        {
            self.level_info.actual = 0;
            godot_dbg!(GameState::Start);
            GameState::Start
        } else {
            GameState::TitleScreen
        }
    }

    fn start_state(&mut self, owner: &Spatial) -> GameState {
        let level_data = &self.level_data[self.level_info.actual as usize];
        self.level_timer = (level_data.boss_strength + level_data.enemies + 1) as f64;
        godot_dbg!(level_data.boss_strength, level_data.enemies);
        godot_dbg!(self.level_timer);
        owner.emit_signal("main_level_name", &[Variant::new(&level_data.location)]);

        // Reset Hud
        let hud = unsafe { owner.get_node_as_instance::<hud::Hud>("Hud").unwrap() };
        hud.map_mut(|x, o| x.set_level(&o, self.level_info.actual))
            .ok()
            .unwrap_or_else(|| godot_print!("Unable to get Enemy"));

        // Start Player
        let player = unsafe {
            owner
                .get_node_as_instance::<player::Player>("Player")
                .unwrap()
        };
        player
            .map_mut(|x, o| x.start(&o, level_data.player_speed))
            .ok()
            .unwrap_or_else(|| godot_print!("Unable to get player"));

        // Start Enemy
        let enemy = unsafe { owner.get_node_as_instance::<enemy::Enemy>("Enemy").unwrap() };
        enemy
            .map_mut(|x, o| x.start(&o, level_data.enemies as i32))
            .ok()
            .unwrap_or_else(|| godot_print!("Unable to get Enemy"));

        godot_dbg!(GameState::Running);
        GameState::Running
    }

    fn running_state(&mut self, owner: &Spatial, input: &Input, delta: f64) -> GameState {
        self.level_timer -= delta;
        if self.level_timer < 0.0 {
            godot_dbg!(GameState::LevelDone);
            GameState::LevelDone
        } else if Input::is_action_just_pressed(input, "ui_cancel", false) {
            let player = unsafe {
                owner
                    .get_node_as_instance::<player::Player>("Player")
                    .unwrap()
            };
            player
                .map(|x, o| x.stop(&o))
                .ok()
                .unwrap_or_else(|| godot_print!("Unable to get player"));
            godot_dbg!(GameState::Setting);
            GameState::Setting
        } else {
            owner.emit_signal("main_level_timer", &[Variant::new(self.level_timer)]);
            GameState::Running
        }
    }
    fn leveldone_state(&mut self, owner: &Spatial) -> GameState {
        /*
        use gdnative::api::File;
        let file = File::new();
        file.open(filepath, File::WRITE).expect(&format!("{} must exist", &filepath));
        file.store_string(data);
        */

        let player = unsafe {
            owner
                .get_node_as_instance::<player::Player>("Player")
                .unwrap()
        };
        player
            .map(|x, o| x.stop(&o))
            .ok()
            .unwrap_or_else(|| godot_print!("Unable to get Player"));

        let enemy = unsafe { owner.get_node_as_instance::<enemy::Enemy>("Enemy").unwrap() };
        enemy
            .map(|x, o| x.stop(&o))
            .ok()
            .unwrap_or_else(|| godot_print!("Unable to get Enemy"));

        let score = unsafe { owner.get_node_as_instance::<score::Score>("Score").unwrap() };
        score
            .map(|x, o| x.stop(&o))
            .ok()
            .unwrap_or_else(|| godot_print!("Unable to stop Score"));

        // update ground
        let ground = unsafe {
            owner
                .get_node_as_instance::<ground::Ground>("Ground")
                .unwrap()
        };

        let index = (self.level_info.actual + 1) as usize;
        if index < self.level_data.len() {
            let level_data = &self.level_data[index];
            ground
                .map_mut(|x, o| {
                    x.start(
                        &o,
                        level_data.color.ground_color,
                        level_data.color.side_color,
                        level_data.color.sky_color,
                        level_data.blocks,
                    )
                })
                .ok()
                .unwrap_or_else(|| godot_print!("Unable to get player"));
        }

        godot_dbg!(GameState::Score);
        GameState::Score
    }

    /// the score state shows the current score from the stage
    /// [`self.leveldone_state`] is the key input Spatial
    fn score_state(&mut self, owner: &Spatial, input: &Input) -> GameState {
        if Input::is_action_just_pressed(input, "ui_accept", false)
            || Input::is_action_just_pressed(input, "ui_shot", false)
        {
            self.level_info.actual += 1;

            if self.level_info.actual == self.level_info.last {
                godot_dbg!(GameState::GameDone);

                // reset score
                let score = unsafe { owner.get_node_as_instance::<score::Score>("Score").unwrap() };
                score
                    .map_mut(|x, o| x.reset_score(&o))
                    .ok()
                    .unwrap_or_else(|| godot_print!("Unable to reset score"));

                /*
                let mut high_score = {
                    serde_json::from_str::<Value>(&self.load_data_as_string(&self.filepath_score))
                    .unwrap()
                };
                high_score["score"] = json!({"david": "123"});
                godot_dbg!(&high_score);
                let new_highscore = serde_json::to_string(&high_score).unwrap();
                self.save_data_from_string(&self.filepath_score, &new_highscore);
                */

                // Reset Hud
                let hud = unsafe { owner.get_node_as_instance::<hud::Hud>("Hud").unwrap() };
                hud.map_mut(|x, o| x.start(&o))
                    .ok()
                    .unwrap_or_else(|| godot_print!("Unable to get Enemy"));

                GameState::GameDone
            } else {
                godot_dbg!(GameState::Start);
                GameState::Start
            }
        } else {
            GameState::Score
        }
    }

    fn game_done_state(&mut self, _owner: &Spatial, input: &Input) -> GameState {
        if Input::is_action_just_pressed(input, "ui_accept", false)
            || Input::is_action_just_pressed(input, "ui_shot", false)
        {
            godot_dbg!(GameState::TitleScreen);
            GameState::TitleScreen
        } else {
            GameState::GameDone
        }
    }
    /*

    fn save_data_from_string(&self, filepath: &String, data: &String) {
        use gdnative::api::File;
        let file = File::new();
        file.open(filepath, File::WRITE)
        .unwrap_or_else(|_| panic!("{} must exist", &filepath));
        godot_dbg!(&file);
        file.store_string(data);
    }
    */
    fn load_data_as_string(&self, filepath: &String) -> String {
        use gdnative::api::File;
        let file = File::new();
        file.open(filepath, File::READ)
            .unwrap_or_else(|_| panic!("{} must exist", &filepath));
        let data: GodotString = file.get_as_text(false);
        data.to_string()
    }
}
