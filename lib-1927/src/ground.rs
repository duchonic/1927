//! the documentation of the map
//! second line
use gdnative::api::*;
use gdnative::prelude::*;
use rand::*;

/// The gournd class
#[derive(NativeClass)]
#[inherit(Spatial)]
pub struct Ground {
    #[property(default = 100.0)]
    speed: f32,
    player_pos: Vector3,
    block_length: f32,
    field_view: i32,
    blocks: i32,
    ground_color: Color,
    side_color: Color,
    sky_color: Color,
}

#[methods]
impl Ground {
    fn new(_owner: &Spatial) -> Ground {
        Ground {
            speed: 10.0,
            player_pos: Vector3::ZERO,
            block_length: 100.0,
            field_view: 5,
            blocks: 0,
            ground_color: Color::from_rgba(0.0, 1.0, 0.5, 1.0),
            side_color: Color::from_rgba(0.0, 1.0, 0.2, 1.0),
            sky_color: Color::from_rgba(1.0, 1.0, 0.5, 1.0),
        }
    }

    #[method]
    fn _ready(&mut self, #[base] owner: TRef<Spatial>) {
        godot_dbg!("init ground");

        let emitter = &mut owner.get_node("/root/World/Player").unwrap();
        let emitter = unsafe { emitter.assume_safe() };

        emitter
            .connect(
                "player_position",
                owner,
                "player_position",
                VariantArray::new_shared(),
                0,
            )
            .unwrap();

        let new_block = (self.player_pos.z / self.block_length) as i32;

        for i in -self.field_view + 1..=1 {
            self.generate_block(&owner, new_block + i);
        }
    }

    #[method]
    fn _process(&mut self, #[base] _owner: &Spatial, _delta: f32) {}

    #[method]
    fn player_position(&mut self, #[base] owner: &Spatial, position: Variant) {
        let new_position = position.try_to::<Vector3>().unwrap();

        if new_position.z < self.player_pos.z
            && new_position.z % self.block_length + self.block_length / 2.0 <= 0.0
            && self.player_pos.z % self.block_length + self.block_length / 2.0 > -0.0
        {
            let new_block = (self.player_pos.z / self.block_length) as i32;
            self.generate_block(owner, new_block - self.field_view);
            self.delete_block(owner, new_block + 1);
        }
        self.player_pos = new_position;
    }

    #[method]
    fn generate_block(&mut self, #[base] owner: &Spatial, block_nr: i32) {
        godot_dbg!(block_nr);
        // first object
        let rigid_body = StaticBody::new();
        // child of rigidbody is collision
        let collision_shape = CollisionShape::new();
        let box_shape = BoxShape::new();
        // child of collision is ground_box
        let ground_box = CSGBox::new();

        rigid_body.set_name(block_nr.to_string());

        rigid_body.set_translation(Vector3 {
            x: 0.0,
            y: 0.0,
            z: block_nr as f32 * self.block_length,
        });

        collision_shape.set_shape(box_shape);

        let mat = SpatialMaterial::new();
        mat.set_albedo(self.ground_color);
        ground_box.set_material(mat);

        collision_shape.add_child(ground_box, false);

        collision_shape.set_scale(Vector3 {
            x: 100.0,
            y: 1.0,
            z: (self.block_length - 30.0) / 2.0,
        });
        rigid_body.add_child(collision_shape, false);

        owner.add_child(rigid_body, false);

        // first object
        let rigid_body = StaticBody::new();
        // child of rigidbody is collision
        let collision_shape = CollisionShape::new();
        let box_shape = BoxShape::new();
        // child of collision is ground_box
        let ground_box = CSGBox::new();

        rigid_body.set_name(format!("side_left_{block_nr}"));

        rigid_body.set_translation(Vector3 {
            x: -130.0,
            y: -2.0,
            z: block_nr as f32 * self.block_length,
        });

        collision_shape.set_shape(box_shape);

        let mat = SpatialMaterial::new();
        mat.set_albedo(self.side_color);
        ground_box.set_material(mat);

        collision_shape.add_child(ground_box, false);

        collision_shape.set_scale(Vector3 {
            x: 10.0,
            y: 1.0,
            z: (self.block_length - 20.0) / 2.0,
        });
        rigid_body.add_child(collision_shape, false);

        owner.add_child(rigid_body, false);

        // first object
        let rigid_body = StaticBody::new();
        // child of rigidbody is collision
        let collision_shape = CollisionShape::new();
        let box_shape = BoxShape::new();
        // child of collision is ground_box
        let ground_box = CSGBox::new();

        rigid_body.set_name(format!("side_right_{block_nr}"));

        rigid_body.set_translation(Vector3 {
            x: 130.0,
            y: -2.0,
            z: block_nr as f32 * self.block_length,
        });

        collision_shape.set_shape(box_shape);

        let mat = SpatialMaterial::new();
        mat.set_albedo(self.side_color);
        ground_box.set_material(mat);

        collision_shape.add_child(ground_box, false);

        collision_shape.set_scale(Vector3 {
            x: 10.0,
            y: 1.0,
            z: (self.block_length - 20.0) / 2.0,
        });
        rigid_body.add_child(collision_shape, false);

        owner.add_child(rigid_body, false);

        for i in 0..self.blocks {
            let rigid_body = StaticBody::new();
            let collision_shape = CollisionShape::new();
            let box_shape = BoxShape::new();

            let ground_box = CSGBox::new();

            rigid_body.set_name(format!("ground_{block_nr}{i}"));

            let mut rng = rand::thread_rng();

            collision_shape.set_rotation_degrees(Vector3 {
                x: rng.gen_range(0.0..90.0),
                y: rng.gen_range(0.0..90.0),
                z: rng.gen_range(0.0..90.0),
            });

            let mat = SpatialMaterial::new();
            mat.set_albedo(Color::from_rgba(1.0, 1.0, 1.0, 0.4));
            ground_box.set_material(mat);

            collision_shape.set_shape(box_shape);
            collision_shape.add_child(ground_box, false);

            collision_shape.set_scale(Vector3::ONE * 10.0);
            rigid_body.translate(Vector3 {
                x: rng.gen_range(-100..100) as f32,
                y: 0.0,
                z: block_nr as f32 * self.block_length
                    + rng.gen_range(-self.block_length / 2.0..self.block_length / 2.0),
            });

            //ground.add_child(ground_box, false);
            rigid_body.add_child(collision_shape, false);
            owner.add_child(rigid_body, false);
        }
    }

    #[method]
    fn delete_block(&mut self, #[base] owner: &Spatial, block_nr: i32) {
        match unsafe { owner.get_node_as::<Spatial>(block_nr.to_string()) } {
            Some(block) => {
                block.queue_free();
            }
            None => {
                println!("block not found");
            }
        }
        match unsafe { owner.get_node_as::<Spatial>(format!("side_left_{block_nr}")) } {
            Some(block) => {
                block.queue_free();
            }
            None => {
                println!("block not found");
            }
        }
        match unsafe { owner.get_node_as::<Spatial>(format!("side_right_{block_nr}")) } {
            Some(block) => {
                block.queue_free();
            }
            None => {
                println!("block not found");
            }
        }
        for i in 0..self.blocks {
            match unsafe { owner.get_node_as::<Spatial>(format!("ground_{block_nr}{i}")) } {
                Some(block) => {
                    block.queue_free();
                }
                None => {
                    println!("block not found");
                }
            }
        }
    }

    #[method]
    //pub fn start(&mut self, #[base] _owner: &Spatial, ground_color: Color) {
    pub fn start(
        &mut self,
        #[base] _owner: &Spatial,
        ground: Color,
        side: Color,
        sky: Color,
        blocks: i32,
    ) {
        self.ground_color = ground;
        self.side_color = side;
        self.sky_color = sky;
        self.blocks = blocks;
    }
}
