use gdnative::api::{Area, RigidBody};
use gdnative::{api::AudioStreamPlayer, prelude::*};

#[derive(Debug)]
pub struct PlayerSetting {
    speed: i64,
    velocity: Vector2,
    acceleration: Vector2,
    breaks: Vector2,
    health: i32,
}

/// The player class
#[derive(NativeClass)]
#[inherit(Spatial)]
#[register_with(Self::register_signals)]
pub struct Player {
    settings: PlayerSetting,
    sound_effect_shot: Option<Ref<AudioStreamPlayer>>,
    sound_effect_plane: Option<Ref<AudioStreamPlayer>>,
}

#[methods]
impl Player {
    fn register_signals(signal_builder: &ClassBuilder<Self>) {
        signal_builder.signal("player_launch_shot").done();
        signal_builder.signal("player_launch_rocket").done();
        signal_builder.signal("player_position").done();
        signal_builder.signal("player_health").done();
    }

    fn new(_owner: &Spatial) -> Self {
        Player {
            settings: PlayerSetting {
                speed: 100,
                velocity: Vector2 { x: 0.0, y: 0.0 },
                acceleration: Vector2 { x: 5.0, y: 2.0 },
                breaks: Vector2 { x: 8.0, y: 2.0 },
                health: 5,
            },
            sound_effect_shot: None,
            sound_effect_plane: None,
        }
    }
    #[method]
    fn _ready(&mut self, #[base] owner: TRef<Spatial>) {
        let player_collision_emitter = &mut owner.get_node("/root/World/Player/Collision").unwrap();
        let player_collision_emitter = unsafe { player_collision_emitter.assume_safe() };

        player_collision_emitter
            .connect(
                "player_hit", // signal
                owner,
                "hit", // connect emitter
                VariantArray::new_shared(),
                0,
            )
            .unwrap();

        owner.translate(Vector3 {
            x: 0.0,
            y: 40.0,
            z: 0.0,
        });

        self.settings.velocity.y = -20.0;
        self.sound_effect_shot = Some(unsafe {
            owner
                .get_node_as::<AudioStreamPlayer>("Shot")
                .unwrap()
                .claim()
        });
        self.sound_effect_plane = Some(unsafe {
            owner
                .get_node_as::<AudioStreamPlayer>("Plane")
                .unwrap()
                .claim()
        });
    }

    #[method]
    fn hit(&mut self, #[base] owner: &Spatial) {
        self.settings.health -= 1;
        owner.emit_signal("player_health", &[Variant::new(self.settings.health)]);
    }

    #[method]
    fn _process(&mut self, #[base] owner: &Spatial, delta: f32) {
        if owner.is_visible() {
            let input = Input::godot_singleton();

            let plane: &Ref<AudioStreamPlayer> = self.sound_effect_plane.as_ref().unwrap();
            unsafe {
                if !plane.assume_safe().is_playing() {
                    plane.assume_safe().play(0.0);
                    plane.assume_safe().set_volume_db(-20.0);
                }
            }

            if Input::is_action_just_pressed(input, "ui_shot", false) {
                let shot = self.sound_effect_shot.as_ref().unwrap();
                unsafe {
                    if !shot.assume_safe().is_playing() {
                        shot.assume_safe().set_volume_db(-20.0);
                        shot.assume_safe().play(0.0);
                    }
                }
                owner.emit_signal("player_launch_shot", &[Variant::new(owner.translation())]);
            }
            if Input::is_action_just_pressed(input, "ui_rocket", false) {
                let shot = self.sound_effect_shot.as_ref().unwrap();
                unsafe {
                    if !shot.assume_safe().is_playing() {
                        shot.assume_safe().play(0.0);
                    }
                }
                owner.emit_signal("player_launch_rocket", &[Variant::new(owner.translation())]);
            }

            owner.emit_signal("player_position", &[owner.translation().to_variant()]);

            if Input::is_action_pressed(input, "ui_right", false) && owner.translation().x < 80.0 {
                self.settings.velocity.x += self.settings.acceleration.x;
            } else if Input::is_action_pressed(input, "ui_left", false)
                && owner.translation().x > -80.0
            {
                self.settings.velocity.x -= self.settings.acceleration.x;
            } else if self.settings.velocity.x > self.settings.acceleration.x {
                self.settings.velocity.x -= self.settings.breaks.x;
            } else if self.settings.velocity.x < -self.settings.acceleration.x {
                self.settings.velocity.x += self.settings.breaks.x;
            } else {
                self.settings.velocity.x = 0.0;
            }
            /*
            if Input::is_action_pressed(input, "ui_down", false) {
                self.settings.velocity.y += self.settings.acceleration.y;
            } else if Input::is_action_pressed(input, "ui_up", false) {
                self.settings.velocity.y -= self.settings.acceleration.y;
            }
            */
            //else if self.velocity.y > self.acceleration.y {
            //    self.velocity.y -= self.breaks.y;
            //} else if self.velocity.y < -self.acceleration.y {
            //    self.velocity.y += self.breaks.y;
            //} else {
            //    self.velocity.y = 0.0;
            //}

            let change = self.settings.velocity * delta;
            owner.translate(Vector3 {
                x: change.x,
                y: 0.0,
                z: change.y,
            });

            let ship: Option<TRef<Spatial>> = unsafe { owner.get_node_as::<Spatial>("RootNode") };
            let ship: TRef<Spatial> = ship.expect("node is present");
            ship.set_rotation_degrees(Vector3 {
                x: self.settings.velocity.y / 10.0,
                y: 0.0,
                z: -self.settings.velocity.x / 4.0,
            });
        } else {
            let plane = self.sound_effect_plane.as_ref().unwrap();
            unsafe {
                if plane.assume_safe().is_playing() {
                    plane.assume_safe().stop();
                }
            }
        }
    }

    #[method]
    pub fn start(&mut self, #[base] owner: &Spatial, speed: i64) {
        //owner.show();
        self.settings.speed = speed;
        self.settings.velocity.y = -speed as f32;
        self.settings.health = 5;
        owner.emit_signal("player_health", &[Variant::new(self.settings.health)]);
    }
    #[method]
    pub fn stop(&self, #[base] _owner: &Spatial) {
        //owner.hide();
    }
}

#[derive(NativeClass)]
#[inherit[Area]]
#[register_with(Self::register_signals)]
pub struct PlayerCollision {}

#[methods]
impl PlayerCollision {
    fn register_signals(signal_builder: &ClassBuilder<Self>) {
        signal_builder.signal("player_hit").done();
    }

    fn new(_: &Area) -> Self {
        PlayerCollision {}
    }

    #[method]
    fn _ready(&mut self, #[base] owner: TRef<Area>) {
        owner
            .connect(
                //the function that connect signal
                "body_entered",     //the name of the signal
                owner,              // target
                "_on_area_entered", // the callback function
                VariantArray::new_shared(),
                0,
            )
            .unwrap();

        godot_print!("hello from PlayerCollision");
    }

    #[method]
    fn _on_area_entered(&self, #[base] owner: &Area, _area: Ref<RigidBody>) {
        owner.emit_signal("player_hit", &[]);
    }
}
