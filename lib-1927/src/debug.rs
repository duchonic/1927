use gdnative::api::*;
use gdnative::prelude::*;

#[derive(NativeClass)]
#[inherit(Label)]
pub struct SignalSubscriber {
    times_received: i32,
}

#[methods]
impl SignalSubscriber {
    fn new(_owner: &Label) -> Self {
        SignalSubscriber { times_received: 0 }
    }

    #[method]
    fn _ready(&mut self, #[base] owner: TRef<Label>) {
        let emitter = &mut owner.get_node("/root/World/Player").unwrap();
        let emitter = unsafe { emitter.assume_safe() };

        godot_dbg!("SignalSubscriber added");
        owner.set_text("texblut");

        emitter
            .connect(
                "player_position",
                owner,
                "notify_with_data",
                VariantArray::new_shared(),
                0,
            )
            .unwrap();
    }

    #[method]
    fn notify(&mut self, #[base] owner: &Label) {
        self.times_received += 1;
        let msg = format!("Received signal \"tick\" {} times", self.times_received);
        owner.set_text(msg);
    }

    #[method]
    fn notify_with_data(&mut self, #[base] owner: &Label, position: Variant) {
        let position = position.try_to::<Vector3>().unwrap();
        let msg = format!("{}", position.z as i32);
        owner.set_text(msg);
    }
}

#[derive(NativeClass)]
#[inherit(Label)]
pub struct Debug2 {}

#[methods]
impl Debug2 {
    fn new(_owner: &Label) -> Self {
        Debug2 {}
    }

    #[method]
    fn _ready(&mut self, #[base] owner: TRef<Label>) {
        let emitter = &mut owner.get_node("/root/World/Bullet").unwrap();
        let emitter = unsafe { emitter.assume_safe() };

        godot_dbg!("SignalSubscriber added");
        owner.set_text("texblut");

        emitter
            .connect(
                "bullet_shots_strength_count",
                owner,
                "notify_with_data",
                VariantArray::new_shared(),
                0,
            )
            .unwrap();
    }

    #[method]
    fn notify_with_data(&mut self, #[base] owner: &Label, _shot: Variant, rocket: Variant) {
        let msg = format!("shots {}", rocket.to::<f64>().unwrap());
        owner.set_text(msg);
    }
}

#[derive(NativeClass)]
#[inherit(ScrollBar)]
pub struct ShotCount {}

#[methods]
impl ShotCount {
    fn new(_owner: &ScrollBar) -> Self {
        ShotCount {}
    }

    #[method]
    fn _ready(&mut self, #[base] owner: TRef<ScrollBar>) {
        let emitter = &mut owner.get_node("/root/World/Bullet").unwrap();
        let emitter = unsafe { emitter.assume_safe() };

        godot_dbg!("SignalSubscriber added");

        emitter
            .connect(
                "bullet_shots_strength_count",
                owner,
                "notify_with_data",
                VariantArray::new_shared(),
                0,
            )
            .unwrap();
    }

    #[method]
    fn notify_with_data(&mut self, #[base] owner: &ScrollBar, shot: Variant, _rocket: Variant) {
        let shot = shot.try_to::<f32>().unwrap() as f64;
        owner.set_value(shot);
    }
}

#[derive(NativeClass)]
#[inherit(ScrollBar)]
pub struct RocketCount {}

#[methods]
impl RocketCount {
    fn new(_owner: &ScrollBar) -> Self {
        RocketCount {}
    }

    #[method]
    fn _ready(&mut self, #[base] owner: TRef<ScrollBar>) {
        let emitter = &mut owner.get_node("/root/World/Bullet").unwrap();
        let emitter = unsafe { emitter.assume_safe() };

        godot_dbg!("SignalSubscriber added");

        emitter
            .connect(
                "bullet_shots_strength_count",
                owner,
                "notify_with_data",
                VariantArray::new_shared(),
                0,
            )
            .unwrap();
    }

    #[method]
    fn notify_with_data(&mut self, #[base] owner: &ScrollBar, _shot: Variant, rocket: Variant) {
        let rocket = rocket.try_to::<f32>().unwrap() as f64;
        owner.set_value(rocket);
    }
}
