use gdnative::api::*;
use gdnative::prelude::*;
use rand::*;

/// The gournd class
#[derive(NativeClass)]
#[inherit(Spatial)]
pub struct Sky {
    #[property(default = 100.0)]
    speed: f32,
    player_pos: Vector3,
    block_length: f32,
    clouds: u32,
    field_view: i32,
}

#[methods]
impl Sky {
    fn new(_owner: &Spatial) -> Sky {
        Sky {
            speed: 10.0,
            player_pos: Vector3::ZERO,
            block_length: 100.0,
            clouds: 13,
            field_view: 5,
        }
    }

    #[method]
    fn _ready(&mut self, #[base] owner: TRef<Spatial>) {
        godot_dbg!("init Sky");

        let emitter = &mut owner.get_node("/root/World/Player").unwrap();
        let emitter = unsafe { emitter.assume_safe() };

        emitter
            .connect(
                "player_position",
                owner,
                "sky_position",
                VariantArray::new_shared(),
                0,
            )
            .unwrap();

        let new_block = (self.player_pos.z / 50.0) as i32;
        for i in -self.field_view + 1..=1 {
            self.generate_block(&owner, new_block + i);
        }
    }

    #[method]
    fn sky_position(&mut self, #[base] owner: &Spatial, position: Variant) {
        let new_position = position.try_to::<Vector3>().unwrap();
        if new_position.z < self.player_pos.z
            && new_position.z % self.block_length + self.block_length / 2.0 <= 0.0
            && self.player_pos.z % self.block_length + self.block_length / 2.0 > -0.0
        {
            let new_block = (self.player_pos.z / self.block_length) as i32;
            self.generate_block(owner, new_block - self.field_view);
            self.delete_sky(owner, new_block + 1);
        }
        self.player_pos = new_position;
    }

    #[method]
    fn generate_block(&mut self, #[base] owner: &Spatial, block_nr: i32) {
        for i in 0..self.clouds {
            let sky = Spatial::new();
            let cloud = CSGBox::new();
            sky.set_name(format!("sky_{block_nr}{i}"));

            let mut rng = rand::thread_rng();
            cloud.set_width(rng.gen_range(20..22) as f64);
            cloud.set_depth(rng.gen_range(20..22) as f64);
            cloud.set_height(rng.gen_range(1..2) as f64);

            sky.translate(Vector3 {
                x: rng.gen_range(-100.0..100.0),
                y: rng.gen_range(100.0..120.0),
                z: block_nr as f32 * self.block_length
                    + rng.gen_range(-self.block_length..self.block_length),
            });

            let mat = SpatialMaterial::new();
            mat.set_albedo(Color::from_rgba(1.0, 1.0, 1.0, 0.4));
            cloud.set_material(mat);

            cloud.set_rotation_degrees(Vector3 {
                x: rng.gen_range(0.0..10.0),
                y: rng.gen_range(0.0..90.0),
                z: rng.gen_range(0.0..10.0),
            });

            sky.add_child(cloud, false);
            owner.add_child(sky, false);
        }
    }

    #[method]
    fn delete_sky(&mut self, #[base] owner: &Spatial, block_nr: i32) {
        for i in 0..self.clouds {
            match unsafe { owner.get_node_as::<Spatial>(format!("sky_{block_nr}{i}")) } {
                Some(block) => {
                    block.queue_free();
                }
                None => {
                    println!("block not found");
                }
            }
        }
    }
}
