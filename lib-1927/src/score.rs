use gdnative::api::*;
use gdnative::prelude::*;

#[derive(NativeClass)]
#[inherit(Spatial)]
#[register_with(Self::register_signals)]
pub struct Score {
    shot_score: i32,
    player_position: Vector3,
    player_score: i32,
    player_combo: i32,
    sound_effect_coin: Option<Ref<AudioStreamPlayer>>,
}

#[methods]
impl Score {
    fn register_signals(signal_builder: &ClassBuilder<Self>) {
        signal_builder.signal("score_player_total").done();
        signal_builder.signal("score_player_combo").done();
    }
    fn new(_owner: &Spatial) -> Self {
        Score {
            shot_score: 0,
            player_position: Vector3::ZERO,
            player_score: 0,
            player_combo: 1,
            sound_effect_coin: None,
        }
    }

    #[method]
    fn _ready(&mut self, #[base] owner: TRef<Spatial>) {
        let bullet_emitter = &mut owner.get_node("/root/World/Bullet").unwrap();
        let bullet_emitter = unsafe { bullet_emitter.assume_safe() };

        self.sound_effect_coin = Some(unsafe {
            owner
                .get_node_as::<AudioStreamPlayer>("Coin")
                .unwrap()
                .claim()
        });

        bullet_emitter
            .connect(
                "bullet_shots_strength",
                owner,
                "set_shot_score",
                VariantArray::new_shared(),
                0,
            )
            .unwrap();

        let enemy_emitter = &mut owner.get_node("/root/World/Enemy").unwrap();
        let enemy_emitter = unsafe { enemy_emitter.assume_safe() };

        enemy_emitter
            .connect(
                "enemy_destroyed",
                owner,
                "set_shot_label",
                VariantArray::new_shared(),
                0,
            )
            .unwrap();

        let player_emitter = &mut owner.get_node("/root/World/Player").unwrap();
        let player_emitter = unsafe { player_emitter.assume_safe() };

        player_emitter
            .connect(
                "player_position",
                owner,
                "set_player_position",
                VariantArray::new_shared(),
                0,
            )
            .unwrap();
    }

    #[method]
    fn _process(&mut self, #[base] owner: &Spatial, _delta: f64) {
        let childrens = owner.get_children();

        for child in &childrens {
            let child = child.try_to_object::<RigidBody>();

            match child {
                Ok(child) => {
                    let child = unsafe { child.assume_safe() };

                    if self.player_position.distance_to(child.translation()) < 5.0 {
                        self.player_combo += 1;
                        owner.emit_signal("score_player_combo", &[Variant::new(self.player_combo)]);
                        child.queue_free();

                        let coin = self.sound_effect_coin.as_ref().unwrap();
                        unsafe {
                            //if !coin.assume_safe().is_playing() {
                            coin.assume_safe().set_volume_db(-20.0);
                            coin.assume_safe().play(0.0);
                            //}
                        }
                    } else if self.player_position.distance_to(child.translation()) < 40.0 {
                        let correction = child.translation().x - self.player_position.x;
                        child.add_force(
                            Vector3 {
                                x: -correction / 2.0,
                                y: 0.0,
                                z: 0.0,
                            },
                            Vector3 {
                                x: 1.0,
                                y: 0.0,
                                z: 0.0,
                            },
                        );
                    } else if self.player_position.z + 20.0 < child.translation().z {
                        child.queue_free();
                        self.player_combo = 1;
                        owner.emit_signal("score_player_combo", &[Variant::new(self.player_combo)]);
                    }
                }
                _ => {
                    print!("");
                }
            }
        }
    }
    #[method]
    fn set_player_position(&mut self, #[base] _owner: &Spatial, position: Variant) {
        self.player_position = position.try_to::<Vector3>().unwrap();
    }
    #[method]
    fn set_shot_score(&mut self, #[base] _owner: &Spatial, score: Variant) {
        self.shot_score = score.try_to::<i32>().unwrap();
    }

    #[method]
    fn set_shot_label(&mut self, #[base] owner: &Spatial, position: Variant) {
        let body = RigidBody::new();

        let mut position = position.try_to::<Vector3>().unwrap();
        position.y = 40.0;
        let label3d = Label3D::new();

        self.player_score += self.player_combo * self.shot_score;
        owner.emit_signal("score_player_total", &[Variant::new(self.player_score)]);

        label3d.set_text(format!("{}", self.shot_score));
        label3d.set_scale(Vector3::ONE * 20.0);
        label3d.set_translation(Vector3 {
            x: 0.0,
            y: 3.0,
            z: 0.0,
        });
        let collision_shape = CollisionShape::new();
        let box_shape = BoxShape::new();
        let item = CSGBox::new();

        collision_shape.set_shape(box_shape);

        let mat = SpatialMaterial::new();
        mat.set_albedo(Color::from_rgba(1.0, 1.0, 0.0, 0.4));
        item.set_material(mat);

        collision_shape.add_child(item, false);
        collision_shape.set_scale(Vector3::ONE * 1.0);
        body.add_child(collision_shape, false);

        body.set_collision_mask(0);
        body.set_collision_layer(0);
        body.set_translation(position);
        body.add_child(label3d, false);
        body.set_angular_velocity(Vector3 {
            x: 0.0,
            y: self.shot_score as f32,
            z: 0.0,
        });
        body.set_linear_velocity(Vector3 {
            x: 0.0,
            y: 0.0,
            z: 40.0,
        });
        body.set_gravity_scale(0.0);
        owner.add_child(body, false);
    }

    #[method]
    pub fn stop(&self, #[base] owner: &Spatial) {
        let childrens = owner.get_children();

        for child in &childrens {
            let child = child.try_to_object::<RigidBody>();
            match child {
                Ok(child) => {
                    let child = unsafe { child.assume_safe() };

                    child.queue_free();
                }
                _ => {
                    print!("unknown object");
                }
            }
        }
    }

    #[method]
    pub fn reset_score(&mut self, #[base] _owner: &Spatial) {
        godot_print!("reset score");
        self.player_combo = 1;
        self.player_score = 0;
    }
}
