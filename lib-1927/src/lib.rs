//! library for a rust shmup with godot
//!
use gdnative::prelude::*;

mod bullet;
mod camera;
mod debug;
mod enemy;
mod ground;
mod hud;
mod main_scene;
mod player;
mod score;
mod sky;

fn init(handle: InitHandle) {
    handle.add_class::<main_scene::Main>();
    handle.add_class::<hud::Hud>();
    handle.add_class::<player::Player>();
    handle.add_class::<player::PlayerCollision>();
    handle.add_class::<debug::SignalSubscriber>();
    handle.add_class::<debug::Debug2>();
    handle.add_class::<debug::ShotCount>();
    handle.add_class::<debug::RocketCount>();
    handle.add_class::<ground::Ground>();
    handle.add_class::<sky::Sky>();
    handle.add_class::<bullet::Bullet>();
    handle.add_class::<camera::Camera>();
    handle.add_class::<enemy::Enemy>();
    handle.add_class::<score::Score>();
}

godot_init!(init);

pub fn add(left: usize, right: usize) -> usize {
    left + right
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn it_works() {
        let result = add(2, 2);
        assert_eq!(result, 4);
    }
}
