use gdnative::api::*;
use gdnative::prelude::*;
use rand::*;

#[derive(NativeClass)]
#[inherit(Spatial)]
#[register_with(Self::register_signals)]
pub struct Bullet {
    shot_timer: f64,
    rocket_timer: f64,
}

#[methods]
impl Bullet {
    fn register_signals(signal_builder: &ClassBuilder<Self>) {
        signal_builder.signal("bullet_shots_strength_count").done();
        signal_builder.signal("bullet_shots_strength").done();
    }
    fn new(_owner: &Spatial) -> Self {
        Bullet {
            shot_timer: 0.0,
            rocket_timer: 0.0,
        }
    }

    #[method]
    fn _ready(&mut self, #[base] owner: TRef<Spatial>) {
        let player_emitter = &mut owner.get_node("/root/World/Player").unwrap();
        let player_emitter = unsafe { player_emitter.assume_safe() };

        player_emitter
            .connect(
                "player_launch_shot",
                owner,
                "make_shot",
                VariantArray::new_shared(),
                0,
            )
            .unwrap();
        player_emitter
            .connect(
                "player_launch_rocket",
                owner,
                "make_rocket",
                VariantArray::new_shared(),
                0,
            )
            .unwrap();

        let enemy_emitter = &mut owner.get_node("/root/World/Enemy").unwrap();
        let enemy_emitter = unsafe { enemy_emitter.assume_safe() };

        enemy_emitter
            .connect(
                "enemy_shot",
                owner,
                "make_enemy_shot",
                VariantArray::new_shared(),
                0,
            )
            .unwrap();
    }

    #[method]
    fn make_shot(&mut self, #[base] owner: &Spatial, data: Variant) {
        let shots = self.shot_timer as i32;
        owner.emit_signal("bullet_shots_strength", &[Variant::new(shots)]);
        self.shot_timer = 1.0;

        for _ in 0..=shots {
            let rigid_body = RigidBody::new();
            let shot = CSGSphere::new();
            let collision_shape = CollisionShape::new();
            let sphere_shape = SphereShape::new();

            rigid_body.set_name("player_launch_shot");

            collision_shape.set_shape(sphere_shape);

            let mat = SpatialMaterial::new();
            mat.set_albedo(Color::from_rgba(1.0, 1.0, 0.8, 0.4));
            shot.set_material(mat);

            collision_shape.add_child(shot, false);
            collision_shape.set_scale(Vector3::ONE);
            let mut rng = rand::thread_rng();

            rigid_body.set_linear_velocity(Vector3 {
                x: rng.gen_range(-1.0..1.0),
                y: rng.gen_range(-1.0..1.0),
                z: -240.0,
            });
            rigid_body.set_gravity_scale(1.8);

            let position: Vector3 = data.try_to::<Vector3>().unwrap()
                - Vector3 {
                    x: 0.0,
                    y: 0.0,
                    z: 5.0,
                };

            rigid_body.translate(position);
            rigid_body.set_mass(1.0);

            rigid_body.add_child(collision_shape, false);
            owner.add_child(rigid_body, false);
        }
    }

    #[method]
    fn make_enemy_shot(&mut self, #[base] owner: &Spatial, data: Variant) {
        let rigid_body = RigidBody::new();
        let shot = CSGSphere::new();
        let collision_shape = CollisionShape::new();
        let sphere_shape = SphereShape::new();

        collision_shape.set_shape(sphere_shape);

        let mat = SpatialMaterial::new();
        mat.set_albedo(Color::from_rgba(1.0, 0.0, 0.0, 1.0));
        shot.set_material(mat);

        collision_shape.add_child(shot, false);
        collision_shape.set_scale(Vector3::ONE);
        let mut rng = rand::thread_rng();

        rigid_body.set_linear_velocity(Vector3 {
            x: rng.gen_range(-1.0..1.0),
            y: rng.gen_range(-1.0..1.0),
            z: 40.0,
        });
        rigid_body.set_gravity_scale(0.3);

        let position: Vector3 = data.try_to::<Vector3>().unwrap();
        rigid_body.translate(position);
        rigid_body.set_mass(1.0);
        rigid_body.set_collision_mask(0);
        rigid_body.set_collision_layer(2);
        rigid_body.add_child(collision_shape, false);

        owner.add_child(rigid_body, false);
    }

    #[method]
    fn make_rocket(&mut self, #[base] owner: &Spatial, data: Variant) {
        let rockets = self.rocket_timer.powf(1.0) as i32;
        self.rocket_timer = 0.0;

        for _ in 0..=rockets {
            let rigid_body = RigidBody::new();
            let shot = CSGBox::new();
            let collision_shape = CollisionShape::new();
            let box_shape = BoxShape::new();

            rigid_body.set_name("bullet");

            collision_shape.set_shape(box_shape);

            let mat = SpatialMaterial::new();
            mat.set_albedo(Color::from_rgba(0.0, 0.0, 0.0, 0.4));
            shot.set_material(mat);

            collision_shape.add_child(shot, false);
            collision_shape.set_scale(Vector3::ONE * 1.2);

            rigid_body.set_linear_velocity(Vector3 {
                x: 0.0,
                y: -10.0,
                z: -150.0,
            });

            rigid_body.set_gravity_scale(4.0);

            let position: Vector3 = data.try_to::<Vector3>().unwrap();
            rigid_body.translate(position);

            rigid_body.add_child(collision_shape, false);
            owner.add_child(rigid_body, false);
        }
    }

    #[method]
    fn _process(&mut self, #[base] owner: &Spatial, delta: f64) {
        self.shot_timer += delta;
        self.rocket_timer += delta / 2.0;
        self.shot_timer = self.shot_timer.clamp(0.0, 4.0);
        self.rocket_timer = self.rocket_timer.clamp(0.0, 4.0);

        owner.emit_signal(
            "bullet_shots_strength_count",
            &[self.shot_timer.to_variant(), self.rocket_timer.to_variant()],
        );

        let childrens = owner.get_children();

        for child in &childrens {
            let child = child.try_to_object::<RigidBody>();

            match child {
                Ok(child) => {
                    let child = unsafe { child.assume_safe() };
                    if child.translation().y <= 3.0 {
                        child.queue_free();
                    }
                }
                _ => {
                    println!("not found");
                }
            }
        }
    }
}
