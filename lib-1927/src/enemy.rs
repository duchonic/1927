//! the enemy module
//!
use gdnative::api::*;
use gdnative::prelude::*;
use rand::*;
use std::f32::consts::PI;

/// The gournd class
#[derive(NativeClass)]
#[inherit(Spatial)]
#[register_with(Self::register_signals)]
pub struct Enemy {
    #[property(default = 100.0)]
    speed: f32,
    player_pos: Vector3,
    block_length: f32,
    enemies: i32,
    timer: f64,
}

#[methods]
impl Enemy {
    fn register_signals(signal_builder: &ClassBuilder<Self>) {
        signal_builder.signal("enemy_destroyed").done();
        signal_builder.signal("enemy_shot").done();
    }
    fn new(_owner: &Spatial) -> Enemy {
        Enemy {
            speed: 10.0,
            player_pos: Vector3::ZERO,
            block_length: 100.0,
            enemies: 3,
            timer: 0.0,
        }
    }

    #[method]
    fn _ready(&mut self, #[base] owner: TRef<Spatial>) {
        godot_dbg!("init Enemy");

        let player_emitter = &mut owner.get_node("/root/World/Player").unwrap();
        let player_emitter = unsafe { player_emitter.assume_safe() };

        player_emitter
            .connect(
                "player_position",
                owner,
                "sky_position",
                VariantArray::new_shared(),
                0,
            )
            .unwrap();

        owner.hide();
    }

    #[method]
    fn _process(&mut self, #[base] owner: &Spatial, delta: f64) {
        if owner.is_visible() {
            let next_slot = (self.timer + delta) as i32;
            let current = self.timer as i32;
            let childrens = owner.get_children();

            if current < next_slot {
                for child in &childrens {
                    let child = child.try_to_object::<RigidBody>();

                    match child {
                        Ok(child) => {
                            let child = unsafe { child.assume_safe() };
                            if self.player_pos.z - child.translation().z < 100.0 {
                                owner.emit_signal(
                                    "enemy_shot",
                                    &[Variant::new(child.translation())],
                                );
                            }
                        }
                        _ => {
                            godot_print!("not found");
                        }
                    }
                }
            }

            if current < next_slot && self.enemies >= 0 {
                let mut rng = rand::thread_rng();
                if rng.gen_range(-1.0..1.0) > 0.0 {
                    self.generate_enemy_ship(
                        owner,
                        self.player_pos
                            + Vector3 {
                                x: 0.0,
                                y: 0.0,
                                z: -200.0,
                            },
                        self.enemies == 0,
                    );
                }

                self.generate_enemy(
                    owner,
                    self.player_pos
                        + Vector3 {
                            x: 0.0,
                            y: 0.0,
                            z: -200.0,
                        },
                    self.enemies == 0,
                );

                self.enemies -= 1;
            }
            self.timer += delta;

            for child in &childrens {
                let child = child.try_to_object::<RigidBody>();

                match child {
                    Ok(child) => {
                        let child = unsafe { child.assume_safe() };

                        if child.name().to_string() == *"boss".to_string() {
                            if child.angular_velocity().length() > 3.0 {
                                owner.emit_signal(
                                    "enemy_destroyed",
                                    &[Variant::new(child.translation())],
                                );
                                child.queue_free();
                            } else if self.player_pos.z - 75.0 < child.translation().z {
                                child.set_axis_lock_linear_z(false);
                                child.set_linear_velocity(Vector3 {
                                    x: 0.0,
                                    y: 0.0,
                                    z: -30.0,
                                });
                            }
                        }

                        if child.name().to_string() == *"boss_ship".to_string() {
                            if child.angular_velocity().length() > 1.0 {
                                owner.emit_signal(
                                    "enemy_destroyed",
                                    &[Variant::new(child.translation())],
                                );
                                child.queue_free();
                            } else if self.player_pos.z - 100.0 < child.translation().z {
                                child.set_axis_lock_linear_z(false);
                                child.set_linear_velocity(Vector3 {
                                    x: 0.0,
                                    y: 0.0,
                                    z: -30.0,
                                });
                            }
                        }
                        if child.angular_velocity().length() > 5.0 {
                            owner.emit_signal(
                                "enemy_destroyed",
                                &[Variant::new(child.translation())],
                            );
                            child.queue_free();
                        } else if self.player_pos.z + self.block_length < child.translation().z {
                            child.queue_free();
                        }
                    }
                    _ => {
                        println!("not found");
                    }
                }
            }
        } else {
            self.enemies = 3;
        }
    }

    #[method]
    fn sky_position(&mut self, #[base] _owner: &Spatial, position: Variant) {
        let new_position = position.try_to::<Vector3>().unwrap();
        self.player_pos = new_position;
    }

    #[method]
    fn generate_enemy_ship(&mut self, #[base] owner: &Spatial, position: Vector3, boss: bool) {
        let rigid_body = RigidBody::new();
        let collision_shape = CollisionShape::new();
        let box_shape = BoxShape::new();

        let ship = CSGBox::new();

        let mut rng = rand::thread_rng();
        rigid_body.translate(
            position
                + Vector3 {
                    x: rng.gen_range(-5.0..5.0),
                    y: -20.0,
                    z: 0.0,
                },
        );

        collision_shape.set_shape(box_shape);

        let mat = SpatialMaterial::new();
        if boss {
            mat.set_albedo(Color::from_rgba(1.0, 1.1, 0.2, 0.4));
            collision_shape.set_scale(Vector3::ONE * 3.0);
            rigid_body.set_mass(30.0);
            rigid_body.set_name("boss_ship");
        } else {
            mat.set_albedo(Color::from_rgba(1.0, 0.1, 0.2, 0.4));
            collision_shape.set_scale(Vector3 {
                x: 4.0,
                y: 1.0,
                z: 4.0,
            });
            rigid_body.set_mass(10.0);
        }
        ship.set_material(mat);

        collision_shape.add_child(ship, false);

        rigid_body.add_child(collision_shape, false);
        rigid_body.set_gravity_scale(0.0);
        rigid_body.set_axis_lock_linear_y(true);
        rigid_body.set_axis_lock_linear_z(true);
        rigid_body.set_collision_layer(2);
        rigid_body.set_rotation(Vector3 {
            x: 0.0,
            y: 0.0,
            z: 0.0,
        });
        rigid_body.set_linear_velocity(Vector3 {
            x: 0.0,
            y: 0.0,
            z: -10.0,
        });
        owner.add_child(rigid_body, false);
    }

    #[method]
    fn generate_enemy(&mut self, #[base] owner: &Spatial, position: Vector3, boss: bool) {
        let rigid_body = RigidBody::new();
        let collision_shape = CollisionShape::new();
        let box_shape = BoxShape::new();

        let ship = CSGBox::new();

        let mut rng = rand::thread_rng();
        rigid_body.translate(
            position
                + Vector3 {
                    x: rng.gen_range(-5.0..5.0),
                    y: 0.0,
                    z: 0.0,
                },
        );

        collision_shape.set_shape(box_shape);

        let mat = SpatialMaterial::new();
        if boss {
            mat.set_albedo(Color::from_rgba(1.0, 1.1, 0.2, 0.4));
            collision_shape.set_scale(Vector3::ONE * 3.0);
            rigid_body.set_mass(30.0);
            rigid_body.set_name("boss");
        } else {
            mat.set_albedo(Color::from_rgba(1.0, 0.1, 0.2, 0.4));
            collision_shape.set_scale(Vector3::ONE * 2.0);
            rigid_body.set_mass(10.0);
        }
        ship.set_material(mat);

        collision_shape.add_child(ship, false);

        rigid_body.add_child(collision_shape, false);
        rigid_body.set_gravity_scale(0.0);
        rigid_body.set_axis_lock_linear_y(true);
        rigid_body.set_axis_lock_linear_z(true);
        rigid_body.set_collision_layer(2);
        rigid_body.set_rotation(Vector3 {
            x: PI * 4.0 / 5.0,
            y: 0.0,
            z: 0.0,
        });
        rigid_body.set_linear_velocity(Vector3 {
            x: 0.0,
            y: 0.0,
            z: 10.0,
        });
        owner.add_child(rigid_body, false);
    }

    #[method]
    fn generate_block(&mut self, #[base] owner: &Spatial, block_nr: i32) {
        for i in 0..self.enemies {
            let rigid_body = RigidBody::new();
            let collision_shape = CollisionShape::new();
            let box_shape = BoxShape::new();

            let ship = CSGBox::new();

            let mut rng = rand::thread_rng();

            rigid_body.set_name(format!("enemy_{block_nr}{i}"));
            rigid_body.translate(Vector3 {
                x: rng.gen_range(-50.0..50.0),
                y: 40.0,
                z: block_nr as f32 * self.block_length,
            });

            collision_shape.set_shape(box_shape);

            let mat = SpatialMaterial::new();
            mat.set_albedo(Color::from_rgba(1.0, 0.1, 0.2, 0.4));
            ship.set_material(mat);

            collision_shape.add_child(ship, false);

            collision_shape.set_scale(Vector3::ONE * 2.0);
            rigid_body.add_child(collision_shape, false);
            rigid_body.set_gravity_scale(0.0);
            rigid_body.set_rotation(Vector3 {
                x: PI * 4.0 / 5.0,
                y: 0.0,
                z: 0.0,
            });
            rigid_body.set_linear_velocity(Vector3 {
                x: 0.0,
                y: 0.0,
                z: 10.0,
            });
            rigid_body.set_mass(10.0);
            owner.add_child(rigid_body, false);
        }
    }

    #[method]
    pub fn start(&mut self, #[base] owner: &Spatial, enemies: i32) {
        self.enemies = enemies;
        owner.show();
    }
    #[method]
    pub fn stop(&self, #[base] owner: &Spatial) {
        owner.hide();

        let childrens = owner.get_children();
        for child in &childrens {
            let child = child.try_to_object::<RigidBody>();

            match child {
                Ok(child) => {
                    let child = unsafe { child.assume_safe() };
                    child.queue_free();
                }
                _ => {
                    println!("not found");
                }
            }
        }
    }
}
