#!/bin/sh
echo "NATIVE BUILD DEBUG"
cd ~/1927/lib-1927
cargo clippy
cargo fmt
cargo build

echo "WASM BUILD"
source ~/emsdk/emsdk_env.sh
export C_INCLUDE_PATH=$EMSDK/upstream/emscripten/cache/sysroot/include
cargo +nightly build --target=wasm32-unknown-emscripten --release

mkdir ~/1927/godot/lib
rm ~/1927/godot/lib/*.wasm
rm ~/1927/godot/lib/*.dylib
mv ~/1927/lib-1927/target/debug/*.dylib ../godot/lib/
mv ~/1927/lib-1927/target/wasm32-unknown-emscripten/release/*.wasm ~/1927/godot/lib
ls -all ../godot/lib

rm ~/1927/godot/wasm/*
godot --export  HTML5 ~/1927/godot/project.godot 

zip -r ~/1927/godot/out/archive.zip ~/1927/godot/wasm

echo "DOCUMENTATION"
cargo test
echo "MAKE DOCUMENTATION"
cargo doc --document-private-items
open ~/1927/lib-1927/target/doc/lib_1927/index.html  