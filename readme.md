# 1927

## try it 

https://duchonic.itch.io/1927-retro-banker

## main_scene

### state

<div class="center">

```mermaid
graph TD;
    Init --> TitleScreen
    TitleScreen --> |CONFIG| Setting
    TitleScreen --> |START| Start
    TitleScreen --> |see high score| HighScore
    LevelDone --> |last boss eliminated| HighScore
    Start --> Running
    Running --> |boss eliminated| LevelDone
    Score --> Start
    LevelDone --> Score
    HighScore --> |esc| TitleScreen
    TitleScreen --> |EXIT| Exit
    Running --> |esc| Setting
    Setting --> |esc| TitleScreen
    Exit --> Exit
```
</div>

## signals

<div class="center">

```mermaid
graph TD;
    main[main_scene] --> | start with level_data /stop | player
    main --> | start/stop | enemy
    player{fa:fa-paper-plane-o player} --> | position | bullet 
    player --> | launch | bullet[fa:fa-rocket bullet]
    player --> |  position | camera[fa:fa-camera camera]
    player --> | position | sky
    player --> | position | ground
    player --> | position | debug
    bullet --> | shot strenth | score[fa:fa-star score]
    player --> | position | score
    enemy{enemy} --> | destoyed | score
    score --> | score and combo | hud[fa:fa-desktop hud]
```

</div>



