#!/bin/sh
cd ~/1927/lib-1927
cargo clippy
cargo fmt
cargo build

mkdir ~/1927/godot/lib
cp ~/1927/lib-1927/target/debug/*.dylib ~/1927/godot/lib
ls -all ~/1927/godot/lib
cd ~/1927/godot
godot -r Main.tscn
